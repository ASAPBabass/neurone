import java.util.ArrayList;
import java.util.Iterator;

/**
 * Created by Bastien on 25/04/2017.
 */
public class Main {
    //class de test
    public static void main(String[] args) {
        int taille = 257;
        DataBase data = new DataBase("train.txt", taille);
        ArrayList<double[]> ListPixel = data.ListStringToDouble(taille);
        Iterator<double[]> it = ListPixel.iterator();
        NeuralNetwork network = new NeuralNetwork(256, 192);
        Neuron[] hidden = new Neuron[192];
        Neuron[] output = new Neuron[10];
        int c = 0;

        while (it.hasNext() && c < 200) {
            c++;
            double[] pixel = it.next();
            network.test(pixel, pixel[256], hidden, output, c);

        }


    }

}
