import java.util.Random;

/**
 * Created by Bastien on 12/04/2017.
 */
public class NeuralNetwork {

    int inputLayerL; //taille de la couche d'entrée
    int hiddenLayerL;
    int outputL = 10;
    private double[][] weightInputToHidden; //poids des connexions entre la couche d'entrée et la couche caché
    private double[][] weightHiddenToOutput; //poids des connexions entre la couche caché et la couche de sortie
    private double[] weightBiaisHidden;
    private double[] weightBiaisOutput;

    public NeuralNetwork(int inputLayerL, int hiddenLayerL) { // (taille de l'entrée, taille de la couche caché)
        this.inputLayerL = inputLayerL;
        this.hiddenLayerL = hiddenLayerL;
        weightInputToHidden = random(new double[inputLayerL][hiddenLayerL]);
        weightHiddenToOutput = random(new double[hiddenLayerL][outputL]);
        weightBiaisHidden = random(new double[hiddenLayerL]);
        weightBiaisOutput = random(new double[outputL]);

    }

    public double[][] random(double[][] f) { //initialise les poids aléatoirement

        for (int i = 0; i < f.length; i++) {
            for (int j = 0; j < f[0].length; j++) {
                f[i][j] = Math.random() - 0.5;

            }

        }
        return f;
    }

    public double[] random(double[] f) { //initialise les poids aléatoirement
        for (int i = 0; i < f.length; i++) {
            f[i] = Math.random() - 0.5;

        }
        return f;
    }

    public int propagation(Neuron[] hidden, Neuron[] output, double[] pixel) { //Propagation du réseau de neurone
        double x = 0;

        //propagation de la couche d'entrée à la couche caché
        for (int i = 0; i < hiddenLayerL; i++) { //verif
            for (int j = 0; j < inputLayerL; j++) {
                x += pixel[j] * weightInputToHidden[j][i];

            }
            x += 1 * weightBiaisHidden[i];
            hidden[i] = new Neuron(x);
            x = 0;
        }

        //propagation de la couche caché à la couche de sortie
        for (int i = 0; i < outputL; i++) { //verif
            for (int j = 0; j < hiddenLayerL; j++) {
                x += hidden[j].getOutput() * weightHiddenToOutput[j][i];
            }
            x += 1 * weightBiaisOutput[i];
            output[i] = new Neuron(x);
            x = 0;
        }
        int sortie = 0;
        x = output[0].getOutput();

        for (int i = 1; i < outputL; i++) {

            if (output[i].getOutput() > x) {
                x = output[i].getOutput();
                sortie = i;
            }
        }
        return sortie;
    }

    public void backPropagation(Neuron[] hidden, Neuron[] output, double[] pixel, double sortie, double res, int t) {

        //float[] ErrorOutput= new float[10];
        double[] deltaOutput = new double[10];
        double[] deltaHidden = new double[hiddenLayerL];
        //float sommeErrorOutput=0;
        double seuil = 0.5;
        double somme = 0;

        for (int i = 0; i < 10; i++) {
            // ErrorOutput[i]= (float) ((1/2)*Math.pow(2,res-sortie));
            deltaOutput[i] = -(res - output[i].getOutput()) * output[i].sigmoidDerive();
            //sommeErrorOutput+=ErrorOutput[i];
        }

        for (int i = 0; i < hiddenLayerL; i++) { //verif
            for (int j = 0; j < 10; j++) {
                somme += weightHiddenToOutput[i][j] * deltaOutput[j];
            }
            deltaHidden[i] = hidden[i].sigmoidDerive() * somme;
        }

        for (int i = 0; i < inputLayerL; i++) {
            for (int j = 0; j < hiddenLayerL; j++) {
                weightInputToHidden[i][j] += seuil * pixel[i] * deltaHidden[j];//Seuil
            }
        }

        for (int j = 0; j < hiddenLayerL; j++) {
            weightBiaisHidden[j] += seuil * 1 * deltaHidden[j];//seuil
        }

        for (int i = 0; i < hiddenLayerL; i++) {
            for (int j = 0; j < 10; j++) {
                weightHiddenToOutput[i][j] += seuil * hidden[i].getOutput() * deltaOutput[j];
            }
        }

        for (int j = 0; j < 10; j++) {
            weightBiaisOutput[j] += seuil * 1 * deltaOutput[j];
        }


    }

    public void backPropagationFinal(Neuron[] hidden, Neuron[] output, double[] pixel, int sortie, double res, int t) {


        double[] deltaOutput = new double[outputL];
        double[] deltaHidden = new double[hiddenLayerL];
        double[] deltaInput = new double[inputLayerL];
        double seuil = 0.5;
        double somme = 0;
        double sommeDeltaOutput = 0;

        for (int i = 0; i < outputL; i++) { // calcule des erreurs de la couche de sortie

            if (i == res) {
                deltaOutput[i] = output[i].getOutput() * (1 - output[i].getOutput()) * (0.9 - output[i].getOutput());
            } else {
                deltaOutput[i] = output[i].getOutput() * (1 - output[i].getOutput()) * (0.1 - output[i].getOutput());
            }
            sommeDeltaOutput += deltaOutput[i];

        }
        System.out.println("Erreur :" + sommeDeltaOutput);


        for (int i = 0; i < hiddenLayerL; i++) { // calcul des erreurs de la couche cachée
            for (int j = 0; j < outputL; j++) {
                somme += weightHiddenToOutput[i][j] * deltaOutput[j];
            }

            deltaHidden[i] = hidden[i].getOutput() * (1 - hidden[i].getOutput()) * somme;
            somme = 0;
        }
        for (int j = 0; j < outputL; j++) {
            somme += weightBiaisOutput[j] * sommeDeltaOutput;
        }
        double deltaBiaisOutput = somme;


        //mise à jour des poids

        for (int i = 0; i < inputLayerL; i++) {
            for (int j = 0; j < hiddenLayerL; j++) {
                weightInputToHidden[i][j] = weightInputToHidden[i][j] + (seuil) * deltaHidden[j] * pixel[i];
            }
        }

        for (int j = 0; j < hiddenLayerL; j++) {
            weightBiaisHidden[j] = weightBiaisHidden[j] + (seuil) * deltaHidden[j] * 1;

        }

        for (int i = 0; i < hiddenLayerL; i++) {
            for (int j = 0; j < outputL; j++) {
                weightHiddenToOutput[i][j] = weightHiddenToOutput[i][j] + (seuil) * deltaOutput[j] * hidden[i].getOutput();


            }
        }

        for (int j = 0; j < outputL; j++) {
            weightBiaisOutput[j] = weightBiaisOutput[j] + (seuil) * deltaBiaisOutput * 1;

        }


    }

    public double sigmoidDerive(double x) {
        return (x * (1.0 - x));
    }

    public void test(double[] pixel, double res, Neuron[] hidden, Neuron[] output, int t) {

        int sortie = propagation(hidden, output, pixel);
        System.out.println("Le résultat trouvé est : " + sortie + " Attendu : " + res);
        backPropagationFinal(hidden, output, pixel, sortie, res, t);

    }

}
