
import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.util.*;
import java.io.*;
import java.lang.*;


/**
 * Created by Bastien on 12/04/2017.
 */
public class DataBase {
    //classe comprenant les interactions avec la base de donnée
    private ArrayList<String[]> base = new ArrayList<String[]>();

    public DataBase(String name, int taille) {
        try {
            String[] registre = new String[taille];
            BufferedReader buf = new BufferedReader(new FileReader(new File(name)));
            while (buf.ready()) {
                registre = buf.readLine().split(",");
                getBase().add(registre);

            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }


    }

    public void ImageToData() { // Conversion d'une image en base de donnée
        //TODO
        //Partie à faire !!
    }

    public void DataToImage(String folder, int taille) { //Création d'images à partir d'une base de donnée, dans le dossier folder

        //Création de l'image
        int counter = 0; //numéro de l'image
        Iterator<String[]> it = base.iterator();
        while (it.hasNext()) {

            String[] registre = it.next();
            double[] pixels = conversionToDouble(registre, taille);

            try {
                counter++;
                BufferedImage img = new BufferedImage(16, 16, BufferedImage.TYPE_BYTE_GRAY);
                File f = new File("D:\\Bastien\\Documents\\" + folder + "\\image" + counter + ".png");
                int compteur2 = 0;
                double[] t = new double[1];
                for (int i = 0; i < 16; ++i) {
                    for (int j = 0; j < 16; ++j) {
                        t[0] = pixels[compteur2];
                        //Initialisation des pixels
                        img.getRaster().setPixel(j, i, t);
                        compteur2++;
                    }
                }
                ImageIO.write(img, "png", f);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

    }

    public ArrayList<float[]> ListStringToFloat() {
        ArrayList<float[]> pixel = new ArrayList<float[]>();
        Iterator<String[]> it = getBase().iterator();
        //System.out.println(it.next()[0]);
        int count = 1;
        while (it.hasNext() && count < 2499) {
            pixel.add(conversionToFloat(it.next()));
            count++;
        }
        return pixel;
    }

    public ArrayList<double[]> ListStringToDouble(int taille) {
        ArrayList<double[]> pixel = new ArrayList<double[]>();
        Iterator<String[]> it = getBase().iterator();
        //System.out.println(it.next()[0]);
        int count = 1;
        while (it.hasNext() && count < 2499) {
            pixel.add(conversionToDouble(it.next(), taille));
            count++;
        }
        return pixel;
    }

    public float[] conversionToFloat(String[] tab) { // conversion d'une tableau de String en float

        float[] pixel = new float[257];
        //System.out.println(Double.valueOf(tab[0]));
        for (int i = 0; i < 257; i++) {
            pixel[i] = Float.valueOf(tab[i]);
        }
        return pixel;
    }

    public double[] conversionToDouble(String[] tab, int taille) { // conversion d'une tableau de String en float

        double[] pixel = new double[taille];
        for (int i = 0; i < taille; i++) {
            pixel[i] = Double.valueOf(tab[i]);
        }
        return pixel;
    }

    public ArrayList<String[]> getBase() {
        return base;
    }

  /*  public void pageHTML(){
        PrintWriter writer = new PrintWriter(new BufferedWriter(new FileWriter(tmp)));

        writer.print("<html><coincoin>" );



        writer.close()
    }
*/

}
