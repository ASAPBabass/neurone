import java.util.Random;

import static java.lang.Math.random;

/**
 * Created by Bastien on 12/04/2017.
 */
public class Neuron {

    private double input;
    private double output;
    Random randon = new Random();

    public Neuron(double input) {
        this.input = input;
        this.output = sigmoid(input);

    }

    public static double sigmoid(double x) {
        return (1.0 / (1.0 + Math.exp(-x)));
    }

    public double sigmoidDerive() {
        return (getOutput() * (1.0 - getOutput()));
    }

    public double getOutput() {
        return output;
    }

    public void setOutput(double output) {
        this.output = output;
    }

    public double getInput() {
        return input;
    }

    public void setInput(float input) {
        this.input = input;
    }
}
